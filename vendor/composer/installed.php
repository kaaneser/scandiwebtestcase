<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'ecde904d70a6803933724a063b22697bde52e534',
        'name' => 'kaan/php-scandi-proje',
        'dev' => true,
    ),
    'versions' => array(
        'heroku/heroku-buildpack-php' => array(
            'pretty_version' => 'v210',
            'version' => '210.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../heroku/heroku-buildpack-php',
            'aliases' => array(),
            'reference' => '9d1aba5f100eeb4c6f07f5f3966a43bdbf87f173',
            'dev_requirement' => true,
        ),
        'kaan/php-scandi-proje' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'ecde904d70a6803933724a063b22697bde52e534',
            'dev_requirement' => false,
        ),
    ),
);
